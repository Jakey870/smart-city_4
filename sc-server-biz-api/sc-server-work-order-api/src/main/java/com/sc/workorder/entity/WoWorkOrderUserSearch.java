package com.sc.workorder.entity;

import com.sc.common.dto.PageDto;

/**
 * @author: wust
 * @date: 2020-04-01 10:18:23
 * @description:
 */
public class WoWorkOrderUserSearch extends WoWorkOrderUser {
    private PageDto pageDto;

    public PageDto getPageDto() {
        return pageDto;
    }

    public void setPageDto(PageDto pageDto) {
        this.pageDto = pageDto;
    }
}