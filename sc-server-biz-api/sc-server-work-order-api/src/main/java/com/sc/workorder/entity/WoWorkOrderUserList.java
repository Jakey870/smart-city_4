package com.sc.workorder.entity;


/**
 * @author: wust
 * @date: 2020-04-01 10:18:23
 * @description:
 */
public class WoWorkOrderUserList extends WoWorkOrderUser {
    private static final long serialVersionUID = 4133154092013889261L;

    private String userLabel;
    private String workOrderTypeLabel;
    private String statusLabel;

    public String getUserLabel() {
        return userLabel;
    }

    public void setUserLabel(String userLabel) {
        this.userLabel = userLabel;
    }

    public String getWorkOrderTypeLabel() {
        return workOrderTypeLabel;
    }

    public void setWorkOrderTypeLabel(String workOrderTypeLabel) {
        this.workOrderTypeLabel = workOrderTypeLabel;
    }

    public String getStatusLabel() {
        return statusLabel;
    }

    public void setStatusLabel(String statusLabel) {
        this.statusLabel = statusLabel;
    }
}