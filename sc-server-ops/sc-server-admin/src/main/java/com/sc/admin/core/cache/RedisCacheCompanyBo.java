/**
 * Created by wust on 2020-03-30 10:44:32
 * Copyright © 2020 wust. All rights reserved.
 */
package com.sc.admin.core.cache;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import com.alibaba.fastjson.JSONObject;
import com.sc.admin.core.dao.SysCompanyMapper;
import com.sc.common.annotations.EnableComplexCaching;
import com.sc.common.entity.admin.company.SysCompany;
import com.sc.common.enums.RedisKeyEnum;
import com.sc.common.cache.CacheAbstract;
import com.sc.common.util.cache.SpringRedisTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author: wust
 * @date: Created in 2020-03-30 10:44:32
 * @description: 公司缓存
 */
@EnableComplexCaching(dependsOnPOSimpleName = "SysCompany")
@Component
public class RedisCacheCompanyBo extends CacheAbstract {
    @Autowired
    private SpringRedisTools springRedisTools;

    @Autowired
    private SysCompanyMapper sysCompanyMapper;

    @Override
    public void init() {
        Set<String> keys = springRedisTools.keys(RedisKeyEnum.REDIS_KEY_HASH_GROUP_COMPANY_BY_NAME.getStringValue().replaceAll("%s_", "*"));
        if (keys != null && keys.size() > 0) {
            springRedisTools.deleteByKey(keys);
        }

        List<SysCompany> companyList = sysCompanyMapper.selectAll();
        if (CollectionUtil.isNotEmpty(companyList)) {
            for (SysCompany company : companyList) {
                cacheByName(company);
            }
        }
    }

    @Override
    public void reset() {

    }

    @Override
    public void add(Object obj) {
        if (obj == null) {
            return;
        }

        SysCompany entity = null;

        if (obj instanceof Long) {
            entity = sysCompanyMapper.selectByPrimaryKey(obj);
        } else if (obj instanceof SysCompany) {
            entity = (SysCompany) obj;
        } else if (obj instanceof Map) {
            entity = JSONObject.parseObject(JSONObject.toJSONString(obj), SysCompany.class);
        }

        if (entity == null) {
            return;
        }

        cacheByName(entity);
    }

    public void batchAdd(List<Object> list) {
        if (CollectionUtil.isNotEmpty(list)) {
            for (Object o : list) {
                add(o);
            }
        }
    }

    public void updateByPrimaryKey(Object primaryKey) {
        SysCompany company = sysCompanyMapper.selectByPrimaryKey(primaryKey);
        if (company != null) {
            cacheByName(company);
        }
    }

    public void batchUpdate(List<Object> list) {
        if(CollectionUtil.isNotEmpty(list)){
            for (Object o : list) {
                updateByPrimaryKey(o);
            }
        }
    }

    public void deleteByPrimaryKey(Object primaryKey) {
        SysCompany company = sysCompanyMapper.selectByPrimaryKey(primaryKey);
        if (company != null) {
            String key2 = String.format(RedisKeyEnum.REDIS_KEY_HASH_GROUP_COMPANY_BY_NAME.getStringValue(), company.getName());
            if (springRedisTools.hasKey(key2)) {
                springRedisTools.deleteByKey(key2);
            }
        }
    }

    public void batchDelete(List<Object> primaryKeys) {
        if (CollectionUtil.isNotEmpty(primaryKeys)) {
            for (Object primaryKey : primaryKeys) {
                deleteByPrimaryKey(primaryKey);
            }
        }
    }


    private void cacheByName(SysCompany company) {
        String key2 = String.format(RedisKeyEnum.REDIS_KEY_HASH_GROUP_COMPANY_BY_NAME.getStringValue(), company.getName());
        if (springRedisTools.hasKey(key2)) {
            springRedisTools.deleteByKey(key2);
        }
        Map mapValue2 = BeanUtil.beanToMap(company);
        springRedisTools.addMap(key2, mapValue2);
    }
}
