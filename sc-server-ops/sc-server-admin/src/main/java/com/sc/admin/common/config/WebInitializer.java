package com.sc.admin.common.config;


import com.sc.common.BaseWebInitializer;
import com.sc.common.context.DefaultBusinessContext;
import com.sc.common.enums.ApplicationEnum;
import com.sc.common.enums.RedisKeyEnum;
import com.sc.common.util.SpringContextHolder;
import com.sc.common.util.cache.SpringRedisTools;
import net.sf.ehcache.CacheManager;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;
import java.io.IOException;
import java.util.Set;

/**
 * 系统启动，初始化相关数据的入口
 * Created by wust on 2019/6/12.
 */
@Component
public class WebInitializer extends BaseWebInitializer implements ApplicationListener<ApplicationReadyEvent> {


    @Override
    public void onApplicationEvent(ApplicationReadyEvent applicationReadyEvent) {
        DefaultBusinessContext.getContext().setDataSourceId(ApplicationEnum.DEFAULT.name());

        ConfigurableApplicationContext configurableApplicationContext = applicationReadyEvent.getApplicationContext();

        resetCached(configurableApplicationContext);

        intHandle(configurableApplicationContext);

        localCachingHandle(configurableApplicationContext);

        distributedCachingHandle(configurableApplicationContext);

        complexCachingHandle(configurableApplicationContext);
    }

    @Override
    protected String getEntityPath() {
        return "com/sc/common/entity/sys/**/*.class";
    }

    @Override
    protected String[] getCacheNames() {
        ClassPathResource classPathResource = new ClassPathResource("ehcache.xml");
        try {
            CacheManager manager = new CacheManager(classPathResource.getInputStream());
            if(manager != null){
                String[] cacheNames = manager.getCacheNames();
                return cacheNames;
            }
        } catch (IOException e) {
        }
        return null;
    }

    @Override
    protected void resetCached(ConfigurableApplicationContext configurableApplicationContext){
        super.resetCached(configurableApplicationContext);

        SpringRedisTools springRedisTools = SpringContextHolder.getBean("springRedisTools");

        Set<String> keys1 = springRedisTools.keys(RedisKeyEnum.REDIS_KEY_PRIMARY_SELECT_BY_PRIMARY_KEY.getStringValue().replace("%s_%s_","*"));
        if(keys1 != null && keys1.size() > 0){
            springRedisTools.deleteByKey(keys1);
        }

        Set<String> keys2 = springRedisTools.keys(RedisKeyEnum.REDIS_KEY_SELECT_ONE.getStringValue().replace("%s_%s_","*"));
        if(keys2 != null && keys2.size() > 0){
            springRedisTools.deleteByKey(keys2);
        }

        Set<String> keys3 = springRedisTools.keys(RedisKeyEnum.REDIS_KEY_SELECT_ONE_EXAMPLE.getStringValue().replace("%s_%s_","*"));
        if(keys3 != null && keys3.size() > 0){
            springRedisTools.deleteByKey(keys3);
        }
    }
}
