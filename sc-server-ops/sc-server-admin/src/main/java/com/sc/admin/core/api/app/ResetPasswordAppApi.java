/**
 * Created by wust on 2019-11-05 11:31:27
 * Copyright © 2019 wust. All rights reserved.
 */
package com.sc.admin.core.api.app;


import com.sc.admin.core.service.impl.SysAccountServiceImpl;
import com.sc.common.annotations.OperationLog;
import com.sc.common.context.DefaultBusinessContext;
import com.sc.common.dto.WebResponseDto;
import com.sc.common.entity.admin.account.SysAccount;
import com.sc.common.enums.ApplicationEnum;
import com.sc.common.enums.OperationLogEnum;
import com.sc.common.util.RC4;
import com.sc.common.util.cache.DataDictionaryUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.Date;

/**
 * @author: wust
 * @date: Created in 2019-11-05 11:31:27
 * @description:
 *
 */
@Api(tags = {"App接口~重置密码（已登录）"})
@RequestMapping("/api/app/v1/ResetPasswordAppApi")
@RestController
public class ResetPasswordAppApi {
    @Autowired
    private SysAccountServiceImpl sysAccountServiceImpl;


    @ApiOperation(value = "重置密码", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name="password",value="登陆口令（使用32位小写MD5密文，如0192023a7bbd73250516f069df18b500）",required=true,paramType="query")
    })
    @OperationLog(moduleName= OperationLogEnum.MODULE_COMMON,businessName="App修改密码",operationType= OperationLogEnum.Update)
    @RequestMapping(value = "",method = RequestMethod.POST,produces ="application/json;charset=utf-8")
    public WebResponseDto resetPassword(@RequestParam("password") String password){
        WebResponseDto responseDto = new WebResponseDto();
        DefaultBusinessContext ctx = DefaultBusinessContext.getContext();

        SysAccount account = DataDictionaryUtil.getAccountByCode(ctx.getAccountCode());
        if(account != null){
            String passwordRC4 = RC4.encry_RC4_string(password, ApplicationEnum.LOGIN_RC4_KEY.getStringValue());
            account.setPassword(passwordRC4);
            account.setModifyId(ctx.getAccountId());
            account.setModifyName(ctx.getAccountName());
            account.setModifyTime(new Date());
            sysAccountServiceImpl.updateByPrimaryKeySelective(account);
        }
        return responseDto;
    }
}
