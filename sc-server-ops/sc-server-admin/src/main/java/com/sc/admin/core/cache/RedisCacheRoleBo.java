/**
 * Created by wust on 2020-03-30 10:44:32
 * Copyright © 2020 wust. All rights reserved.
 */
package com.sc.admin.core.cache;


import cn.hutool.core.collection.CollectionUtil;
import com.sc.admin.core.dao.SysRoleMapper;
import com.sc.common.annotations.EnableComplexCaching;
import com.sc.common.cache.CacheAbstract;
import com.sc.common.util.cache.SpringRedisTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.List;

/**
 * @author: wust
 * @date: Created in 2020-03-30 10:44:32
 * @description: 角色缓存
 *
 */
@EnableComplexCaching(dependsOnPOSimpleName = "SysRole")
@Component
public class RedisCacheRoleBo extends CacheAbstract {
    @Autowired
    private SpringRedisTools springRedisTools;

    @Autowired
    private SysRoleMapper sysRoleMapper;

    @Override
    public void init() {

    }

    @Override
    public void reset() {

    }

    @Override
    public void add(Object obj){

    }

    @Override
    public void batchAdd(List<Object> list){

    }

    @Override
    public void updateByPrimaryKey(Object primaryKey){

    }

    @Override
    public void batchUpdate(List<Object> list){

    }

    @Override
    public void deleteByPrimaryKey(Object primaryKey){
    }

    @Override
    public void batchDelete(List<Object> primaryKeys){
        if(CollectionUtil.isNotEmpty(primaryKeys)){
            for (Object primaryKey : primaryKeys) {
                deleteByPrimaryKey(primaryKey);
            }
        }
    }
}
