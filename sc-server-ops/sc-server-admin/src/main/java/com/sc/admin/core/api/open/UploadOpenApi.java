/**
 * Created by wust on 2019-12-13 15:36:59
 * Copyright © 2019 wust. All rights reserved.
 */
package com.sc.admin.core.api.open;

import com.sc.common.annotations.OpenApi;
import com.sc.common.annotations.OperationLog;
import com.sc.common.dto.WebResponseDto;
import com.sc.common.enums.OperationLogEnum;
import com.sc.common.service.MinioStorageApiService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author: wust
 * @date: Created in 2019-12-13 15:36:59
 * @description: 开放api上传文件接口
 *
 */
@Api(tags = {"开放接口~上传文件"})
@OpenApi
@RequestMapping("/api/open/v1/UploadOpenApi")
@RestController
public class UploadOpenApi {
    @Autowired
    private MinioStorageApiService minioStorageApiService;

    @ApiOperation(value = "上传文件", httpMethod = "POST")
    @OperationLog(moduleName= OperationLogEnum.MODULE_COMMON,businessName="上传文件",operationType= OperationLogEnum.Upload)
    @RequestMapping(value = "",method = RequestMethod.POST,consumes = "multipart/*",headers = "content-type=multipart/form-data",produces ="application/json;charset=utf-8")
    public WebResponseDto uploadFile(@RequestParam(value = "file") MultipartFile multipartFile){

       WebResponseDto responseDto = new WebResponseDto();
       return responseDto;
    }
}
