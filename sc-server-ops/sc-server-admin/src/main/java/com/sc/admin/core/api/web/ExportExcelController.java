package com.sc.admin.core.api.web;

import com.alibaba.fastjson.JSONObject;
import com.sc.common.annotations.OperationLog;
import com.sc.common.annotations.WebApi;
import com.sc.common.context.DefaultBusinessContext;
import com.sc.common.dto.ExcelDto;
import com.sc.common.dto.WebResponseDto;
import com.sc.common.entity.admin.importexport.SysImportExport;
import com.sc.common.enums.OperationLogEnum;
import com.sc.common.service.ExportExcelService;
import com.sc.common.util.CodeGenerator;
import com.sc.common.util.MyStringUtils;
import com.sc.mq.producer.Producer4routingKey;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.*;
import java.util.Date;

/**
 * Created by wust on 2019/5/24.
 */
@WebApi
@RequestMapping("/web/v1/ExportExcelController")
@RestController
public class ExportExcelController {

    @Autowired
    private ExportExcelService exportExcelServiceImpl;

    @Autowired
    private Producer4routingKey producer4routingKey;

    @Value("${spring.rabbitmq.exportexcel.exchange.name}")
    private String exchangeName;

    @Value("${spring.rabbitmq.exportexcel.routing-key}")
    private String routingKey;

    @Autowired
    private Environment environment;

    @OperationLog(moduleName= OperationLogEnum.MODULE_COMMON,businessName="导出Excel",operationType= OperationLogEnum.Export)
    @RequestMapping(value = "/exportExcel", method = RequestMethod.POST)
    public WebResponseDto exportExcel(@RequestBody ExcelDto excelDto) {
        WebResponseDto mm = new WebResponseDto();
        if (excelDto == null) {
            mm.setFlag(WebResponseDto.INFO_WARNING);
            mm.setMessage("导出参数[XML Name, Excel Version, Module Name]不能为空。");
            return mm;
        } else if (StringUtils.isBlank(MyStringUtils.null2String(excelDto.getXmlName()))) {
            mm.setFlag(WebResponseDto.INFO_WARNING);
            mm.setMessage("XML Name参数不能少噢。");
            return mm;
        } else if (StringUtils.isBlank(MyStringUtils.null2String(excelDto.getFileType()))) {
            mm.setFlag(WebResponseDto.INFO_WARNING);
            mm.setMessage("File Type参数不能少噢。");
            return mm;
        }

        DefaultBusinessContext ctx = DefaultBusinessContext.getContext();

        String batchNo = CodeGenerator.genImportExportCode();
        SysImportExport sysImportExport = new SysImportExport();
        sysImportExport.setBatchNo(batchNo);
        sysImportExport.setModuleName(excelDto.getXmlName());
        sysImportExport.setStartTime(new Date());
        sysImportExport.setOperationType("A100602");
        sysImportExport.setStatus("A100501");
        sysImportExport.setCreaterId(ctx.getAccountId());
        sysImportExport.setCreaterName(ctx.getAccountName());
        sysImportExport.setCreateTime(new Date());


        excelDto.setBatchNo(batchNo);

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("excelDto",excelDto);
        jsonObject.put("sysImportExport",sysImportExport);
        jsonObject.put("ctx",DefaultBusinessContext.getContext());
        jsonObject.put("spring.application.name",environment.getProperty("spring.application.name"));
        producer4routingKey.send(exchangeName,routingKey,jsonObject);
        return mm;
    }
}
