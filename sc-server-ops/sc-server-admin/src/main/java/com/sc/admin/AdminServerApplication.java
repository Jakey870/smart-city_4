package com.sc.admin;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import tk.mybatis.spring.annotation.MapperScan;


@EnableEurekaClient
@EnableTransactionManagement
@MapperScan(basePackages = {"com.sc.common.dao","com.sc.admin.core.dao"})
@SpringBootApplication(scanBasePackages = {"com.sc"})
public class AdminServerApplication {
    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(AdminServerApplication.class);
        app.setWebApplicationType(WebApplicationType.REACTIVE);
        SpringApplication.run(AdminServerApplication.class, args);
    }
}
