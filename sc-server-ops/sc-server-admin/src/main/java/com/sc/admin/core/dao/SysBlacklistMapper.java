package com.sc.admin.core.dao;


import com.sc.common.mapper.IBaseMapper;
import com.sc.common.entity.admin.blacklist.SysBlacklist;

public interface SysBlacklistMapper extends IBaseMapper<SysBlacklist> {
}