package com.sc.admin.core.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.sc.admin.core.dao.*;
import com.sc.admin.core.dao.*;
import com.sc.admin.core.service.SysUserService;
import com.sc.common.context.DefaultBusinessContext;
import com.sc.common.entity.admin.account.SysAccount;
import com.sc.common.mapper.IBaseMapper;
import com.sc.common.dto.WebResponseDto;
import com.sc.common.entity.admin.company.SysCompany;
import com.sc.common.entity.admin.project.SysProject;
import com.sc.common.entity.admin.user.SysUser;
import com.sc.common.entity.admin.user.SysUserList;
import com.sc.common.entity.admin.userorganization.SysUserOrganizationSearch;
import com.sc.common.service.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.Date;
import java.util.List;

/**
 * Created by wust on 2019/4/18.
 */
@Service("sysUserServiceImpl")
public class SysUserServiceImpl extends BaseServiceImpl<SysUser> implements SysUserService {
    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private SysImportExportMapper sysImportExportMapper;

    @Autowired
    private SysMenuMapper sysMenuMapper;

    @Autowired
    private SysCompanyMapper sysCompanyMapper;

    @Autowired
    private SysProjectMapper sysProjectMapper;

    @Autowired
    private SysAccountMapper sysAccountMapper;


    @Override
    protected IBaseMapper getBaseMapper() {
        return sysUserMapper;
    }


    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto create(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        JSONObject jsonObject = (JSONObject)obj;

        SysUser user = jsonObject.getObject("user",SysUser.class);
        sysUserMapper.insert(user);

        SysAccount account = jsonObject.getObject("account", SysAccount.class);
        sysAccountMapper.insert(account);
        return responseDto;
    }

    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto update(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        DefaultBusinessContext ctx = DefaultBusinessContext.getContext();

        SysUser user = (SysUser)obj;

        SysUser userOld = sysUserMapper.selectByPrimaryKey(user.getId());
        if(!userOld.getLoginName().equals(user.getLoginName())){
            SysCompany companySearch = new SysCompany();
            companySearch.setAdminAccount(user.getLoginName());
            SysCompany company = sysCompanyMapper.selectOne(companySearch);
            if(company != null){
                company.setAdminAccount(user.getLoginName());
                company.setModifyId(ctx.getAccountId());
                company.setModifyName(ctx.getAccountName());
                company.setModifyTime(new Date());
                sysCompanyMapper.updateByPrimaryKeySelective(company);
            }


            SysProject projectSearch = new SysProject();
            projectSearch.setAdminAccount(user.getLoginName());
            SysProject project = sysProjectMapper.selectOne(projectSearch);
            if(project != null){
                project.setAdminAccount(user.getLoginName());
                project.setModifyId(ctx.getAccountId());
                project.setModifyName(ctx.getAccountName());
                project.setModifyTime(new Date());
                sysProjectMapper.updateByPrimaryKeySelective(project);
            }
        }

        SysAccount account =  sysAccountMapper.selectByPrimaryKey(user.getId());
        account.setAccountName(user.getRealName());
        account.setContactNumber(user.getMobilePhone());
        account.setEmail(user.getEmail());
        account.setModifyId(ctx.getAccountId());
        account.setModifyName(ctx.getAccountName());
        account.setModifyTime(new Date());
        sysAccountMapper.updateByPrimaryKeySelective(account);


        user.setModifyId(ctx.getAccountId());
        user.setModifyName(ctx.getAccountName());
        user.setModifyTime(new Date());
        sysUserMapper.updateByPrimaryKeySelective(user);
        return responseDto;
    }

    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto delete(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        DefaultBusinessContext ctx = DefaultBusinessContext.getContext();

        SysAccount account = sysAccountMapper.selectByPrimaryKey(obj);
        if(account != null){
            account.setIsDeleted(1);
            account.setModifyId(ctx.getAccountId());
            account.setModifyName(ctx.getAccountName());
            account.setModifyTime(new Date());
            sysAccountMapper.updateByPrimaryKeySelective(account);
        }

        SysUser user = sysUserMapper.selectByPrimaryKey(obj);
        if(user != null){
            user.setIsDeleted(1);
            user.setModifyId(ctx.getAccountId());
            user.setModifyName(ctx.getAccountName());
            user.setModifyTime(new Date());
            sysUserMapper.updateByPrimaryKeySelective(user);
        }
        return responseDto;
    }


    @Override
    public List<SysUserList> listPageByParentOrganization(SysUserOrganizationSearch search,int pageNum,int pageSize) {
        return sysUserMapper.listPageByParentOrganization(search,pageNum,pageSize);
    }
}
