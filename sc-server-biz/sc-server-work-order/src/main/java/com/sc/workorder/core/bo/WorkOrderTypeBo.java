/**
 * Created by wust on 2020-04-03 14:06:55
 * Copyright © 2020 wust. All rights reserved.
 */
package com.sc.workorder.core.bo;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.sc.common.context.DefaultBusinessContext;
import com.sc.workorder.entity.WoWorkOrderType;
import com.sc.workorder.core.dao.WoWorkOrderTypeMapper;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import tk.mybatis.mapper.entity.Example;
import java.util.List;

/**
 * @author: wust
 * @date: Created in 2020-04-03 14:06:55
 * @description:
 *
 */
@Component
public class WorkOrderTypeBo {

    @Autowired
    private WoWorkOrderTypeMapper woWorkOrderTypeMapper;

    public JSONArray buildCascader(){
        final JSONArray jsonArray = new JSONArray();

        DefaultBusinessContext ctx = DefaultBusinessContext.getContext();

        Example example = new Example(WoWorkOrderType.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("projectId",ctx.getProjectId());
        criteria.andEqualTo("companyId",ctx.getBranchCompanyId());
        List<WoWorkOrderType> woWorkOrderTypes =  woWorkOrderTypeMapper.selectByExample(example);
        if(CollectionUtils.isNotEmpty(woWorkOrderTypes)){
            for (WoWorkOrderType woWorkOrderType : woWorkOrderTypes) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("value",woWorkOrderType.getId());
                jsonObject.put("label",woWorkOrderType.getName());
                jsonArray.add(jsonObject);
            }
        }
        return jsonArray;
    }
}
