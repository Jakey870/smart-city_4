package com.sc.demo2.test;

import org.joda.time.DateTime;

public class Test {
    public static void main(String[] args){
        System.out.println(new DateTime(1598394707105L).toString("yyyy-MM-dd HH:mm:ss"));
        System.out.println(new DateTime(System.currentTimeMillis()).toString("yyyy-MM-dd"));
        System.out.println(System.currentTimeMillis());
    }
}
