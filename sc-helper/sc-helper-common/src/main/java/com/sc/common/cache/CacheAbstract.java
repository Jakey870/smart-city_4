package com.sc.common.cache;

import java.util.List;

public abstract class CacheAbstract {
    public abstract void init();
    public abstract void reset();
    public abstract void add(Object obj);
    public abstract void batchAdd(List<Object> list);
    public abstract void updateByPrimaryKey(Object primaryKey);
    public abstract void batchUpdate(List<Object> list);
    public abstract void deleteByPrimaryKey(Object primaryKey);
    public abstract void batchDelete(List<Object> primaryKeys);
}
