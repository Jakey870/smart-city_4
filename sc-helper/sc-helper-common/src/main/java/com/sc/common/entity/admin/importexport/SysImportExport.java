package com.sc.common.entity.admin.importexport;


import com.sc.common.entity.BaseEntity;

import java.util.Date;

/**
 * Created by wust on 2019/5/20.
 */
public class SysImportExport extends BaseEntity {
    private static final long serialVersionUID = 8972450411783576856L;

    /** 批次号 **/
    private String batchNo;
    /** 附件名称 **/
    private String name;
    /** 模块名称 **/
    private String moduleName;
    /** 描述 **/
    private String description;
    /** 操作类型：导入，导出 **/
    private String operationType;
    /** 当前状态：1，执行中；2，全部导入成功；3，部分导入成功；4，导入失败。 **/
    private String status;
    /** 导入非成功时的错误消息 **/
    private String msg;
    /** 开始时间 **/
    private Date startTime;
    /** 结束时间 **/
    private Date endTime;
    /** 附件大小KB **/
    private Integer size;
    /** minio文件服务器对应的id**/
    private Long fileId;

    public String getBatchNo() {
        return batchNo;
    }

    public void setBatchNo(String batchNo) {
        this.batchNo = batchNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOperationType() {
        return operationType;
    }

    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }
}
