/**
 * Created by wust on 2019-10-21 14:28:17
 * Copyright © 2019 wust. All rights reserved.
 */
package com.sc.common.entity.admin.notification;

/**
 * @author: wust
 * @date: Created in 2019-10-21 14:28:17
 * @description:
 *
 */
public class SysNotificationList extends SysNotification {
    private static final long serialVersionUID = 1248401570741343712L;

    private String statusLabel;

    private String receiverTypeLabel;

    private String priorityLevelLabel;

    private String typeLabel;

    private String receiverLabel;

    private String sendChannelLabel;

    public String getStatusLabel() {
        return statusLabel;
    }

    public void setStatusLabel(String statusLabel) {
        this.statusLabel = statusLabel;
    }

    public String getReceiverTypeLabel() {
        return receiverTypeLabel;
    }

    public void setReceiverTypeLabel(String receiverTypeLabel) {
        this.receiverTypeLabel = receiverTypeLabel;
    }

    public String getPriorityLevelLabel() {
        return priorityLevelLabel;
    }

    public void setPriorityLevelLabel(String priorityLevelLabel) {
        this.priorityLevelLabel = priorityLevelLabel;
    }

    public String getTypeLabel() {
        return typeLabel;
    }

    public void setTypeLabel(String typeLabel) {
        this.typeLabel = typeLabel;
    }

    public String getReceiverLabel() {
        return receiverLabel;
    }

    public void setReceiverLabel(String receiverLabel) {
        this.receiverLabel = receiverLabel;
    }

    public String getSendChannelLabel() {
        return sendChannelLabel;
    }

    public void setSendChannelLabel(String sendChannelLabel) {
        this.sendChannelLabel = sendChannelLabel;
    }
}
