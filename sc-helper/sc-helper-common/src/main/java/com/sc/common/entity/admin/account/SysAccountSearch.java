package com.sc.common.entity.admin.account;

import com.sc.common.dto.PageDto;

/**
 * @author: wust
 * @date: 2019-12-27 11:55:31
 * @description:
 */
public class SysAccountSearch extends SysAccount {
    private PageDto pageDto;

    public PageDto getPageDto() {
        return pageDto;
    }

    public void setPageDto(PageDto pageDto) {
        this.pageDto = pageDto;
    }
}