package com.sc.common.entity.admin.apptoken;

/**
 * Created by wust on 2019/4/18.
 */
public class SysAppTokenList extends SysAppToken {
    private static final long serialVersionUID = 2242327663629366838L;

    private String statusLabel;
    private String agentName;
    private String parentCompanyName;
    private String branchCompanyName;
    private String projectName;


    public String getStatusLabel() {
        return statusLabel;
    }

    public void setStatusLabel(String statusLabel) {
        this.statusLabel = statusLabel;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public String getParentCompanyName() {
        return parentCompanyName;
    }

    public void setParentCompanyName(String parentCompanyName) {
        this.parentCompanyName = parentCompanyName;
    }

    public String getBranchCompanyName() {
        return branchCompanyName;
    }

    public void setBranchCompanyName(String branchCompanyName) {
        this.branchCompanyName = branchCompanyName;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }
}
