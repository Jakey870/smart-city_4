package ${DaoPackageName};

import ${BasePackageName}.common.mapper.IBaseMapper;
import ${EntityPackageName}.${EntityName};

/**
 * @author: ${Author}
 * @date: ${DateTime}
 * @description:
 */
public interface ${EntityName}Mapper extends IBaseMapper<${EntityName}>{
}