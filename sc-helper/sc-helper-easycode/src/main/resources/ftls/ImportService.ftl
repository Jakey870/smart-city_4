package ${InterfacePackageName};

import com.alibaba.fastjson.JSONObject;
import ${BasePackageName}.common.dto.WebResponseDto;

/**
 * @author: ${Author}
 * @date: ${DateTime}
 * @description:
 */
public interface ${ClassName}ImportService {
    WebResponseDto importByExcel(JSONObject jsonObject);
}
