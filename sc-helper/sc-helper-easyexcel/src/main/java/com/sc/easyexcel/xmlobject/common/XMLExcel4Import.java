package com.sc.easyexcel.xmlobject.common;
/**
 * Created by wust on 2017/5/8.
 */

import cn.hutool.core.collection.CollectionUtil;
import com.sc.easyexcel.ConfigDefinitionBean;
import java.util.List;

/**
 *
 * Function:
 * Reason:
 * Date:2017/5/8
 * @author wust
 */
public class XMLExcel4Import extends ConfigDefinitionBean {

    private static final long serialVersionUID = -6700074651889442998L;
    private String attributeId;

    private List<XMLSheet4Import> xmlSheet4ImportList;

    public String getAttributeId() {
        return attributeId;
    }

    public void setAttributeId(String attributeId) {
        this.attributeId = attributeId;
    }

    public List<XMLSheet4Import> getXmlSheet4ImportList() {
        return xmlSheet4ImportList;
    }

    public void setXmlSheet4ImportList(List<XMLSheet4Import> xmlSheet4ImportList) {
        this.xmlSheet4ImportList = xmlSheet4ImportList;
    }

    public boolean isEmpty(){
        if(attributeId == null
                && CollectionUtil.isEmpty(xmlSheet4ImportList)){
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return "XMLExcel4Import{" +
                "attributeId='" + attributeId + '\'' +
                ", xmlSheet4ImportList=" + xmlSheet4ImportList +
                '}';
    }
}
